How to use

Run createProject.sh using 
    ./createProject.sh

You need to provide a name and directory.
If no directory is provided it will be created as folder in current directory

To build the project the following steps are needed
    ./config.sh
    ./build.sh
    ./install.sh

After the build step the programm can be run using
    ./run.sh