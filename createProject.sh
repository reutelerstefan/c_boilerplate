#!/bin/bash

# Set project name
read -p "Enter project name: " project_name

# Read target folder path (optional)
read -p "Enter target folder path (leave blank for current directory): " target_folder

# Use current directory as default target folder if none provided
if [ -z "$target_folder" ]; then
  target_folder="$(pwd)/projects"
fi


# Create subfolder with project name within the target folder
mkdir -p "$target_folder/$project_name/src"
mkdir -p "$target_folder/$project_name/Adder"
mkdir -p "$target_folder/$project_name/tests"
pwd
# Copy CMakeLists.txt to target folder
cp CMakeLists.txt "$target_folder/$project_name/CMakeLists.txt"
cp License.txt "$target_folder/$project_name"


# Copy main.c to target folder
cp -r src "$target_folder/$project_name"
cp -r Adder "$target_folder/$project_name"
cp -r tests "$target_folder/$project_name"
cp -r external "$target_folder/$project_name"


# Copy additional bash scripts to target folder
cp run.sh config.sh build.sh install.sh test.sh "$target_folder/$project_name/"

# Copy & Rename .H.IN Config File
cp PROJECT_NAMEConfig.h.in "$target_folder/$project_name/$project_name""Config.h.in"

# Replace project name in CMakeLists.txt in the target folder
sed -i "s/PROJECT_NAME/$project_name/g" "$target_folder/$project_name/CMakeLists.txt"

# Replace project name in sh-files in the target folder
sed -i "s/PROJECT_NAME/$project_name/" "$target_folder/$project_name/run.sh"
sed -i "s/PROJECT_NAME/$project_name/" "$target_folder/$project_name/test.sh"

sed -i 's/PROJECT_NAME/$project_name/g' "$target_folder/$project_name/$project_name""Config.h.in"

# Print project summary
echo "Project summary:"
echo "  Project name: $project_name"
echo "  Target folder: $target_folder/$project_name"

cd $target_folder/$project_name